# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .action import Action3
from mud.events import CutWithEvent

class CutWithAction(Action3):
	EVENT = CutWithEvent
	RESOLVE_OBJECT = "resolve_for_operate"
	RESOLVE_OBJECT2 = "resolve_for_use"
	ACTION = "cut-with"
