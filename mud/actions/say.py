# -*- coding: utf-8 -*-
#==============================================================================

from .action import Action3
from mud.events import SayEvent

class SayAction(Action3):
    EVENT = SayEvent
    ACTION = "say"
    RESOLVE_OBJECT = "resolve_for_operate"

    def __init__(self, subject, object):
        super().__init__(subject, "mur", object)

    def resolve_object2(self):
        return self.object2

