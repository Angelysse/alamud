# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .event import Event3

class CutWithEvent(Event3):
	NAME = "cut-with"
	
	def perform(self):
		if not self.object2.has_prop("able-to-cut") or not self.object.has_prop("cutable"):
			self.fail()
			return self.inform("cut-with.failed")
		self.inform("cut-with")
